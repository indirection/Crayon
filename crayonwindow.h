#ifndef CRAYONWINDOW_H
#define CRAYONWINDOW_H

#include <QMainWindow>
#include "ui/clienttable.h"

namespace Ui {
class CrayonWindow;
}

class CrayonWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CrayonWindow(QWidget* parent = nullptr);
    ~CrayonWindow();

private:
    Ui::CrayonWindow *ui;
};

#endif // CRAYONWINDOW_H
