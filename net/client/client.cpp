#include "client.h"

Client::Client()
{
    // we'd receive this with some boost timer timeout and try again
    info_ << new QStandardItem("192.168.1.71");
    info_ << new QStandardItem("dosto@evsky");
    info_ << new QStandardItem("Linux");
}
/*
Client::Client(ip::tcp::socket socket)
    : socket_{std::move(socket)} {

}
*/

const QList<QStandardItem*>& Client::info() const {
    return info_;
}

std::shared_ptr<Client> Client::get() {
    return shared_from_this();
}
