#ifndef CLIENT_H
#define CLIENT_H

#include <memory>
#include <QString>
#include <QList>
#include <QStandardItem>
#include <boost/asio.hpp>

using namespace boost::asio;

class Client : public std::enable_shared_from_this<Client>
{
public:
    //Client(ip::tcp::socket socket);

    /*
     * This only exists because the client needs to be
     * instantiated before it can receieve relevant connection info.
    */
    Client();

    const QList<QStandardItem*> &info() const;

    std::shared_ptr<Client> get();

private:
   // ip::tcp::socket socket_;
    QList<QStandardItem*> info_;
};

#endif // CLIENT_H
