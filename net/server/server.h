#ifndef SERVER_H
#define SERVER_H

#include <boost/asio.hpp>

using namespace boost::asio;
using boost_err = boost::system::error_code;

class Server
{
public:
    explicit Server(uint16_t port);

    Server(const Server&) = delete;
    Server& operator= (const Server&) = delete;

    void start();

    uint16_t port() const;

private:
    io_service io_service_;
    ip::tcp::acceptor acceptor_;
    ip::tcp::socket socket_;
    // cluster (or should table manage cluster?)

    void asyncAccept();
};

#endif // SERVER_H
