#include "server.h"

Server::Server(uint16_t port = 1337)
    : io_service_{}, acceptor_{io_service_}, socket_{io_service_} {
    const ip::tcp::endpoint kEndpoint(ip::tcp::v4(), port);
    acceptor_.open(kEndpoint.protocol());
    acceptor_.bind(kEndpoint);
    acceptor_.listen(10);
}

void Server::start()
{
    io_service_.run();
}

uint16_t Server::port() const
{
    return acceptor_.local_endpoint().port();
}

void Server::asyncAccept()
{
    acceptor_.async_accept(socket_, [this](const boost_err& e){
        if(!e)
        {
          //  cluster_.addClient(std::make_shared<Client>(std::move(socket_)));
        }

        // accept another client
        asyncAccept();
    });
}
