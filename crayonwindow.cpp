#include "crayonwindow.h"
#include "ui_crayonwindow.h"

CrayonWindow::CrayonWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CrayonWindow)
{
    ui->setupUi(this);

    /*
     * TODO: create derived tab widget class
     * so that we can handle the deletion of tabs that
     * we delegate to an internal signal/slot (promotion).
    */
    ui->tabs->addTab(new ClientTable, "Clients");
}

CrayonWindow::~CrayonWindow()
{
    delete ui;
}
