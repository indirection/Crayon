#include "clienttable.h"

ClientTable::ClientTable()
{
    const QList<QString> kHeaders = { "IP", "Username", "OS", "Ping" };

    for(auto i = 0; i < kHeaders.size(); i++)
        model_.setHorizontalHeaderItem(i, new QStandardItem(kHeaders[i]));


    // set model
    this->setModel(&model_);

    // stretch all columns
    for (auto i = 0; i < horizontalHeader()->count(); i++)
    {
        horizontalHeader()->setSectionResizeMode(
            i, QHeaderView::Stretch);
    }

    setSelectionBehavior(SelectionBehavior::SelectRows);
    verticalHeader()->hide();

    Client client {};
    model_.appendRow(client.info());

}

