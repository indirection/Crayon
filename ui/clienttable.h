#ifndef CLIENTTABLE_H
#define CLIENTTABLE_H

#include <QTableView>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QHeaderView>

#include "../net/client/client.h"

class ClientTable : public QTableView
{
public:
    ClientTable();

    ClientTable(const ClientTable&) = delete;
    ClientTable& operator= (const ClientTable&) = delete;

private:
    QStandardItemModel model_;
};

#endif // CLIENTTABLE_H
